/*get  project start here*/

function getProjects(proj_id) {
    var url = "../../comments/getProjects";
    var table = '<thead><tr><th>#</th><th>Project</th><th>ProjectType</th>' +
        '<th>Manager</th><th>Team Leader</th><th>Start Date </th><th>Dead Line</th><th>Add On</th><th>Action</th>' +
        ' </tr></thead><tbody>';
    var tbody = "";
    $.post(url,{"proj_id":proj_id},function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var table ='<thead><tr><th>#</th><th>Project</th><th>Type</th><th>Manager</th><th>Team Leader</th>' +
            '<th>Start Date </th><th>Dead Line</th><th>Closing Date</th><th>Milestone</th><th>Comments</th><th>Action</th></tr></thead><tbody>';
        var tbody = "";
        if(Status){
            var j = 0;
            var projectData = data.data;
            for(var i=0; i < projectData.length; i++){
                j = i+1;
                tbody = tbody + '<tr><td>'+ j+'</td><td>'+projectData[i].proj_name+'<small></small></td>' +
                    ' <td>'+projectData[i].proj_type+'</td><td>'+projectData[i].proj_manager+'</td>' +
                    '<td>'+projectData[i].proj_team_leader+'</td><td>' +projectData[i].proj_start_date+
                    '</td><td>'+projectData[i].proj_dead_line+'</td><td>'+projectData[i].proj_closing_date+
                    '</td><td><span class="projectComm" onclick=projectMilestone("'+projectData[i].proj_id+'");>' +
                    ' Milestone <i class="fa fa-eye"></i></span></td>' +
                    '<td><span class="projectComm" onclick=projectComments("'+projectData[i].proj_id+'")> Comments <i class="fa fa-eye"></i></span>' +
                    '</td><td><a onclick=confirmDeleteProject("'+projectData[i].proj_id+'")><button class="btn' +
                    ' btn-danger btn-sm" type="button"><i class="fa fa-trash text-navy project-fa">' +
                    '</i></button></a>' +
                    '&nbsp;&nbsp;<a onclick=editProject("'+projectData[i].proj_id+'","'+i+'")><button class="btn' +
                    ' btn-primary btn-sm" type="button"><i class="fa fa-edit text-navy project-fa">' +
                    '</i></button></a></td></tr>';
            }
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable();

        }
        $("#projectData").html(table+tbody+"</tbody>");
        $("#projectData").dataTable();

    });
}

/*get  project end here*/