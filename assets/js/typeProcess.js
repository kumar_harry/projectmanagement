/**
 * Created by SachTech on 28-12-2017.
 */

var types = [];

window.onload = function () {
  getAllTypes();
};

function getAllTypes() {
    var url = 'projecttype/types';
    var th_ = '<thead><tr><th>#</th><th>Name</th><th>Created AT</th><th>Updated AT</th><th>Action</th></tr></thead>';
    var td_='<tbody>';
    $.get(url,function (data) {
       console.log('data --- '+JSON.stringify(data));
       var status = data.Status;
       var message = data.Message;
       if(status) {
          types = data.data;
          for(var i=0;i<types.length;i++) {
              td_ = td_+'<td>'+(i+1)+'</td><td>'+types[i].type_name+'</td><td>'+types[i].type_created_at+'</td>'+
                  '<td>'+types[i].type_updated_at+'</td><td><button class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i></button>' +
                  '<button class="btn btn-danger btn-sm"><i class="fa fa-trash-o"></i></button></td>';
          }
           $("#typeData").html(th_+td_+'</tbody>');
       }
        $("#typeData").html(th_+td_+'</tbody>');
        $("#typeData").dataTable();
    });
}

function createType() {
    $("#main_row1").html("<div class='col-md-12 no-padding' style='border-bottom: 1px #eee solid'><label>Create</label></div><div class='col-md-12 no-padding' style='margin-top: 2%'>" +
        "<div class='col-md-6 no-padding'><label>Title</label><input type='text' placeholder='Enter project type' class='form-control'/></div><div class='col-md-6 no-padding' style='padding-left:1% !important; '>" +
        "<label>Status</label><select id='type_status' class='form-control'><option value='A'>Active</option><option value='D'>Disabled</option></select></div>" +
        "</div><div class='col-md-12 no-padding' style='margin-top: 1%'><label>Description</label><textarea class='form-control' style='resize: none' ></textarea></div>"+
        "<div class='col-md-12 no-padding' style='margin-top: 2%'><button class='btn btn-primary btn-sm'><i class='fa fa-floppy-o'></i> Save</button><button class='btn btn-default btn-sm' data-dismiss='modal' style='margin-left: 1%'>" +
        "<i class='fa fa-trash-o'></i> Cancel</button><label id='error' style='margin-left: 1%'></label></div>");
    $("#myModal").modal("show");

}