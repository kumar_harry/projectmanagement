/* array of all data < -- */
var projectData = [];
/* // > array of all data  */


/*get all project start here*/
function getAllProjects() {
    var url = "dashboard/getAllProjects";
    var table = '<thead><tr><th>#</th><th>Project</th><th>ProjectType</th>' +
        '<th>Manager</th><th>Team Leader</th><th>Start Date </th><th>Dead Line</th><th>Add On</th><th>Action</th>' +
        ' </tr></thead><tbody>';
    var tbody = "";
    $.post(url,function (data) {
        var Status = data.Status;
        var Message = data.Message;
        var table ='<thead><tr><th>#</th><th>Project</th><th>Type</th><th>Manager</th><th>Team Leader</th>' +
            '<th>Start Date </th><th>Dead Line</th><th>Closing Date</th><th>Milestone</th><th>Comments</th><th>Action</th></tr></thead><tbody>';
        var tbody = "";
        if(Status == "Success"){
            projectData = data.AllProject;
            var j = 0;
             projectData = data.AllProject;
            for(var i=0; i < projectData.length; i++){
                j = i+1;
                tbody = tbody + '<tr><td>'+ j+'</td><td>'+projectData[i].proj_name+'<small></small></td>' +
                    ' <td>'+projectData[i].proj_type+'</td><td>'+projectData[i].proj_manager+'</td>' +
                    '<td>'+projectData[i].proj_team_leader+'</td><td>' +projectData[i].proj_start_date+
                    '</td><td>'+projectData[i].proj_dead_line+'</td><td>'+projectData[i].proj_closing_date+
                    '</td><td><span class="projectComm" onclick=projectMilestone("'+projectData[i].proj_id+'");>' +
                    ' Milestone <i class="fa fa-eye"></i></span></td>' +
                    '<td><span class="projectComm" onclick=projectComments("'+projectData[i].proj_id+'")> Comments <i class="fa fa-eye"></i></span>' +
                    '</td><td><a onclick=confirmDeleteProject("'+projectData[i].proj_id+'")><button class="btn' +
                    ' btn-danger btn-sm" type="button"><i class="fa fa-trash text-navy project-fa">' +
                    '</i></button></a>' +
                    '&nbsp;&nbsp;<a onclick=editProject("'+projectData[i].proj_id+'","'+i+'")><button class="btn' +
                    ' btn-primary btn-sm" type="button"><i class="fa fa-edit text-navy project-fa">' +
                    '</i></button></a></td></tr>';
            }
            $("#projectData").html(table+tbody+"</tbody>");
            $("#projectData").dataTable();

        }
        $("#projectData").html(table+tbody+"</tbody>");
        $("#projectData").dataTable();

    });
}
/*get all project end here*/

/*for cereate project start here*/
function createProject() {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Create Project</h3><p>Create Project today' +
        ' for more expirience.</p><p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row">' +
        '<div class="col-sm-4"><div class="form-group"><label>Project Name</label> <input type="text" ' +
        'placeholder="Project Name" class="form-control" id="projectName"></div></div>' +
        '<div class="col-sm-4"><div class="form-group">' +
        '<label>Project ProjectType</label><select class="form-control" id="projectType"><option>Select Project ProjectType</option>' +
        '<option value="php">Php</option><option value="ios">ios</option><option value="android">Android</option></select>' +
        '</div></div>' +
        '<div class="col-sm-4"><div class="form-group">' +
        '<label>Project Manager</label><select class="form-control" id="projectManager"><option>Project Manager</option>' +
        '<option value="kapil">Kapil</option><option value="Avtar">Avtar</option><option value="sourav">Sourav</option></select>' +
        '</div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-4"><div class="form-group"><label>Project Team Leader</label><select class="form-control"' +
        ' id="projectLeader"><option>Project Team Leader</option><option value="kapil">Kapil</option><option value="avtar">Avtar' +
        '</option><option value="sourav">Sourav</option></select></div></div>' +
        '<div class="col-sm-4">' +
        '<div class="form-group projectDate"><label>Project Start Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="startDate" type="text" class="form-control" placeholder="start date"></div></div>' +
        '</div>' +
        '<div class="col-sm-4"><div class="form-group projectDate"><label>Project Deadline Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="deadlineDate" type="text" class="form-control" placeholder="deadline date"></div></div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-4"><div class="form-group projectDate" ><label>Project Closing Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="closingDate" type="text" class="form-control" placeholder="closing date"></div>' +
        '</div></div>' +
        '<div class="col-sm-8"><div class="form-group">' +
        '<label>Project Team</label>' +
        '<div><select id="projectTeam" data-placeholder="Choose Team..." class="chosen-select" multiple style="width:350px;" tabindex="4">' +
        '<option value="1">Ravikant</option>' +
        '<option value="2">Sourav</option><option value="3">Nardeep</option>' +
        '</select></div></div>' +
        '</div>' +
        '<div class="col-sm-12"><button class="btn btn-md btn-primary pull-right" style="margin-top: 25px" ' +
        'onclick="projectCreate()" type="button">' +
        '<strong>Submit</strong>' +
        '</button>' +
        '</div></div>');
    $("#myModal").modal("show");
    $('.projectDate .input-group.date').datepicker({
        startView: 1,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd-mm-yyyy"
    });
    $('.chosen-select').chosen({width: "100%"});
}
function projectCreate() {
    var projectName     =  $("#myModal #projectName").val();
    var projectType     =  $("#myModal #projectType").val();
    var projectManager  =  $("#myModal #projectManager").val();
    var projectLeader   =  $("#myModal #projectLeader").val();
    var projectTeam     =  $("#myModal #projectTeam").val();
    var startDate       =  $("#myModal #startDate").val();
    var deadlineDate    =  $("#myModal #deadlineDate").val();
    var closingDate     =  $("#myModal #closingDate").val();
    var currentDate     =  moment().format('DD-MM-YYYY');
    projectTeam = projectTeam.toString();
    if(projectName == "" || projectType == ""){
        $("#err_message").html("please fill required fields");
        $("#err_message").css("color","red");
        return false;
    }
    var url = "dashboard/createProject";
    $.post(url,{"projectName":projectName,"projectType":projectType,"projectManager":projectManager,
    "projectLeader":projectLeader,"projectTeam":projectTeam,"startDate":startDate,"deadlineDate":deadlineDate,
    "closingDate":closingDate,"currentDate":currentDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){
            alert(Status+" " +Message);
            $("#myModal").modal("hide");
            getAllProjects();
        }
        else{
            alert(Status+" " +Message);
        }
    });
}
/*for cereate project end here*/

/*for delete project start here*/
function confirmDeleteProject(proj_id) {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Delete Project</h3>' +
        '<p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<label>Are you sure want to delete this project ?</label></div></div></div>' +
        '<hr><div class="row"><div class="col-sm-12"><div class="form-group">' +
        '<button data-dismiss="modal" class="btn btn-md btn-primary pull-right" style="margin: 0 5px" ' +
        ' type="button"><strong>Cancel</strong></button><button class="btn btn-md btn-danger pull-right"' +
        'style="margin: 0 5px"  type="button" onclick=projectDelete("'+proj_id+'");><strong>Submit</strong></button></div></div></div>');
    $("#myModal").modal("show");
}
function projectDelete(proj_id) {
    var url = "dashboard/deleteProject";
    $.post(url,{"proj_id":proj_id},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){
            alert(Status +" "+Message);
            $("#myModal").modal("hide");
            getAllProjects();
        }
        else{
            alert(Status +" "+Message);
        }
    });
}
/*for delete project end   here*/

/*edit projcet strat here*/
function editProject(proj_id,index) {
    $(".modal-body").html('<div class="row"><h3 class="m-t-none m-b">Update Project</h3>' +
        '<p style="color: red" id="err_message"></p></div><hr>' +
        '<div class="row">' +
        '<div class="col-sm-4"><div class="form-group"><label>Project Name</label> <input type="text" ' +
        'placeholder="Project Name" class="form-control" id="projectName" value="'+projectData[index].proj_name+'"></div></div>' +
        '<div class="col-sm-4"><div class="form-group">' +
        '<label>Project Type</label><select class="form-control" id="projectType"><option>Select Project Type</option>' +
        '<option value="php">Php</option><option value="ios">ios</option><option value="android">Android</option></select>' +
        '</div></div>' +
        '<div class="col-sm-4"><div class="form-group">' +
        '<label>Project Manager</label><select class="form-control" id="projectManager"><option>Project Manager</option>' +
        '<option value="kapil">Kapil</option><option value="Avtar">Avtar</option><option value="sourav">Sourav</option></select>' +
        '</div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-4"><div class="form-group"><label>Project Team Leader</label><select class="form-control"' +
        ' id="projectLeader"><option>Project Team Leader</option><option value="kapil">Kapil</option><option value="avtar">Avtar' +
        '</option><option value="sourav">Sourav</option></select></div></div>' +
        '<div class="col-sm-4">' +
        '<div class="form-group projectDate"><label>Project Start Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="startDate" type="text" class="form-control" placeholder="start date" ' +
        'value="'+projectData[index].proj_start_date+'"></div></div>' +
        '</div>' +
        '<div class="col-sm-4"><div class="form-group projectDate"><label>Project Deadline Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="deadlineDate" type="text" class="form-control" placeholder="deadline date"' +
        ' value="'+projectData[index].proj_dead_line+'"></div></div></div>' +
        '</div>' +
        '<div class="row">' +
        '<div class="col-sm-4"><div class="form-group projectDate" ><label>Project Closing Date</label>' +
        '<div class="input-group date"><span class="input-group-addon"><i class="fa fa-calendar"></i></span>' +
        '<input id="closingDate" type="text" class="form-control" placeholder="closing date" ' +
        'value="'+projectData[index].proj_closing_date+'"></div>' +
        '</div></div>' +
        '<div class="col-sm-8"><div class="form-group">' +
        '<label>Project Team</label>' +
        '<div><select id="projectTeam" data-placeholder="Choose Team..." class="chosen-select" multiple style="width:350px;" tabindex="4">' +
        '<option value="1">Ravikant</option>' +
        '<option value="2">Sourav</option><option value="3">Nardeep</option>' +
        '</select></div></div>' +
        '</div>' +
        '<div class="col-sm-12"><button class="btn btn-md btn-primary pull-right" style="margin-top: 25px" ' +
        ' type="button" onclick=projectEdit("'+proj_id+'")>' +
        '<strong>Submit</strong>' +
        '</button>' +
        '</div></div>');
    $("#myModal").modal("show");
    $('#projectType option[value="'+projectData[index].proj_type+'"]').attr("selected", "selected");
    $('#projectManager option[value="'+projectData[index].proj_manager+'"]').attr("selected", "selected");
    $('#projectLeader option[value="'+projectData[index].proj_team_leader+'"]').attr("selected", "selected");
    $('.projectDate .input-group.date').datepicker({
        startView: 1,
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: "dd-mm-yyyy"
    });
    $('.chosen-select').chosen({width: "100%"});
}
function projectEdit(proj_id) {
    var projectName     =  $("#myModal #projectName").val();
    var projectType     =  $("#myModal #projectType").val();
    var projectManager  =  $("#myModal #projectManager").val();
    var projectLeader   =  $("#myModal #projectLeader").val();
    var projectTeam     =  $("#myModal #projectTeam").val();
    var startDate       =  $("#myModal #startDate").val();
    var deadlineDate    =  $("#myModal #deadlineDate").val();
    var closingDate     =  $("#myModal #closingDate").val();

    projectTeam = projectTeam.toString();
    if(projectName == "" || projectType == ""){
        $("#err_message").html("please fill required fields");
        $("#err_message").css("color","red");
        return false;
    }
    var url = "dashboard/updateProject";
    $.post(url,{"proj_id":proj_id,"projectName":projectName,"projectType":projectType,"projectManager":projectManager,
        "projectLeader":projectLeader,"projectTeam":projectTeam,"startDate":startDate,"deadlineDate":deadlineDate,
        "closingDate":closingDate},function (data) {
        var Status  = data.Status;
        var Message = data.Message;
        if(Status == "Success"){
            alert(Status+" " +Message);
            $("#myModal").modal("hide");
            getAllProjects();
        }
        else{
            alert(Status+" " +Message);
        }
    });
}
/*edit projcet end  here*/
function projectMilestone(proj_id) {
    alert(proj_id);
}
function projectComments(proj_id) {
    window.location = "comments/view/"+proj_id;
}