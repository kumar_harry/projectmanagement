/**
 * Created by SachTech on 26-12-2017.
 */
var users = [];

function onLoginPress() {
    var url = "../user/login";
    var email_id = $("#email_id").val();
    var password = $("#password").val();

    if(email_id === '' || password === '') {
        $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label></div>' +
            '<div class="col-md-12" style="margin-top: 2%"><p>Please enter all fields</p></div><div class="col-md-12">' +
            '<button class="btn btn-default btn-sm btn pull-right" onclick=onDismissDialog("showMessage"); >Cancel</button></div>');
        $("#showMessage").modal("show");
        return false;
    }

    $.post(url,{email_id:email_id,password:password},function (data) {
        console.log('response -- '+data);
        var status = data.Status;
        var message = data.Message;
        if(!status) {
            $("#main_row").html('<div class="col-md-12" style="border-bottom: 1px solid #eee;"><label>Error</label></div>' +
                '<div class="col-md-12" style="margin-top: 2%"><p>'+message+'</p></div><div class="col-md-12">' +
                '<button class="btn btn-default btn-sm btn pull-right" onclick=onDismissDialog("showMessage"); >Cancel</button></div>');
            $("#showMessage").modal("show");
            return false;
        }
        window.location = "../";


    });
}

function onDismissDialog(id) {
    $("#"+id).modal("hide");
}


function getAllUsers() {
    var url = "../user/users_data";
    var th_ = '<thead><tr><th>#</th><th>Name</th><th>Email</th><th>Designation</th><th>Role</th><th style="text-align: center">Operations</th></tr></thead>';
    var td_ = '<tbody>';

    $.post(url,function (data) {
       console.log(JSON.stringify(data));
       var status = data.Status;
       var message = data.Message;
       if(status) {
           users = data.data;
           for(var i=0;i<users.length;i++) {
             var obj = users[i];
             td_ = td_+'<tr><td>'+(i+1)+'</td><td>'+obj.emp_name+'</td><td>'+obj.emp_email+'</td><td>'+obj.emp_designation+'</td>' +
                 '<td>'+obj.emp_role+'</td><td style="text-align: center"><button class="btn-primary btn-sm" onclick="editEmployee('+i+');"><i class="fa fa-pencil"></i></button>' +
                 '&nbsp;&nbsp;<button class="btn-danger btn-sm" onclick="deleteEmployee('+i+');"><i class="fa fa-trash-o"></i></button></td></tr>';
           }
           $("#userData").html(th_+td_+'</tbody>');

       }

    });
    $("#userData").html(th_+td_+'</tbody>');
    $("#userData").dataTable();
}

function createEmployee() {
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Create</label></div> <div class="col-md-12" style="margin-top: 2%">' +
        '<div class="col-md-4"><label>Name</label><input type="text" placeholder="Enter employee name" id="emp_name" class="form-control"/></div>' +
        '<div class="col-md-4"><label>Email</label><input type="text" placeholder="Enter employee email" id="emp_email" class="form-control"/></div>' +
        '<div class="col-md-4"><label>Password</label><input type="password" placeholder="Enter employee password" id="emp_password" class="form-control"/></div>' +
        '</div><div class="col-md-12" style="margin-top: 2%"><div class="col-md-4"><label>Designation</label><select id="emp_desg" class="form-control"><option value="node">NODE JS</option><option value="php">PHP</option>' +
        '<option value="android">Android</option><option value="ios">IOS</option></select></div>' +
        '<div class="col-md-4"><label>Role</label><select id="emp_role" class="form-control"><option value="manager">Project Manager</option><option value="teamleader">Team Leader</option>' +
        '<option value="developer">Developer</option></select></div><div class="col-md-4"><label>Status</label><select id="emp_status" class="form-control"><option value="A">Active</option><option value="D">Disabled</option>' +
        '</select></div></div></div>' +
        '<div class="col-md-12" style="margin-top: 2%;margin-left: 14px;"><button class="btn btn-primary btn-sm" onclick="onCreateEmployee();"><i class="fa fa-floppy-o"></i> Save</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick=onDismissDialog("myModal");><i class="fa fa-trash-o"></i> Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}

function onCreateEmployee() {
    var emp_name = $("#emp_name").val();
    var emp_email = $("#emp_email").val();
    var emp_password = $("#emp_password").val();
    var emp_desg = $("#emp_desg").val();
    var emp_role = $("#emp_role").val();
    var emp_status = $("#emp_status").val();
    var created_at = moment().format('DD-MM-YYYY hh:mm');
    if(emp_name === '' || emp_email === '' || emp_password === '') {
        $("#error").html("Please enter all fields");
        $("#error").css("color","red");
        return false;
    }
    var emp_data  = {emp_name:emp_name,emp_email:emp_email,emp_password:emp_password,emp_desg:emp_desg,
        emp_role:emp_role,emp_status:emp_status,emp_type:'user',emp_created_at:created_at};
    var url = '../user/user_create';
    $.post(url,emp_data,function (data) {
     var status = data.Status;
     var message = data.Message;
     if(status) {
         $("#myModal").modal("hide");
         getAllUsers();
     }
     else{
         $("#error").html(message);
         $("#error").css("color","red");
     }

    });

}

function editEmployee(index) {

    var emp_id = users[index].emp_id;
    var emp_name = users[index].emp_name;
    var emp_email = users[index].emp_email;
    var emp_desg = users[index].emp_designation;
    var emp_role = users[index].emp_role;
    var emp_status = users[index].emp_status;


    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Update</label></div> <div class="col-md-12" style="margin-top: 2%">' +
        '<div class="col-md-4"><label>Name</label><input type="text" placeholder="Enter employee name" id="emp_name" class="form-control" value='+emp_name+'></div>' +
        '<div class="col-md-4"><label>Email</label><input type="text" placeholder="Enter employee email" id="emp_email" class="form-control" value='+emp_email+' disabled="disabled"></div>' +
        '<div class="col-md-4"><label>Designation</label><select id="emp_desg" class="form-control"><option value="node">NODE JS</option><option value="php">PHP</option>' +
        '<option value="android">Android</option><option value="ios">IOS</option></select></div></div>' +
        '<div class="col-md-12" style="margin-top: 2%"><div class="col-md-4"><label>Role</label><select id="emp_role" class="form-control"><option value="manager">Project Manager</option><option value="teamleader">Team Leader</option>' +
        '<option value="developer">Developer</option></select></div><div class="col-md-4"><label>Status</label><select id="emp_status" class="form-control"><option value="A">Active</option><option value="D">Disabled</option>' +
        '</select></div></div></div>' +
        '<div class="col-md-12" style="margin-top: 2%;margin-left: 14px;"><button class="btn btn-primary btn-sm" onclick="onUpdateEmployee('+emp_id+');"><i class="fa fa-floppy-o"></i> Save</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick=onDismissDialog("myModal");><i class="fa fa-trash-o"></i> Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
    $("#emp_desg").val(emp_desg);
    $("#emp_role").val(emp_role);
    $("#emp_status").val(emp_status);

}

function onUpdateEmployee(id) {
    var emp_name = $("#emp_name").val();
    var emp_email = $("#emp_email").val();
    var emp_desg = $("#emp_desg").val();
    var emp_role = $("#emp_role").val();
    var emp_status = $("#emp_status").val();

    if(emp_name === '' || emp_email === '' ) {
        $("#error").html("Please enter all fields");
        $("#error").css("color","red");
        return false;
    }

    var emp_data  = {emp_name:emp_name,emp_email:emp_email,emp_desg:emp_desg,
        emp_role:emp_role,emp_status:emp_status,emp_id:id};

    var url = '../user/user_update';
    $.post(url,emp_data,function (data) {
        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllUsers();
        }
        else{
            $("#error").html(message);
            $("#error").css("color","red");
        }

    });

}

function deleteEmployee(index) {
    var emp_id = users[index].emp_id;
    $("#main_row1").html('<div class="col-md-12" style="border-bottom: 1px #eee solid"><label>Delete</label></div> <div class="col-md-12" style="margin-top: 2%;">' +
        '<p>Would you like to remove employee (y/n) ?</p></div><div class="col-md-12" style="margin-top: 2%;padding-top: 2%;border-top:1px #eee solid"><button class="btn btn-danger btn-sm" onclick="onDeleteEmployee('+emp_id+');"><i class="fa fa-floppy-o"></i> Remove</button>' +
        '&nbsp;&nbsp;<button class="btn btn-default btn-sm" onclick=onDismissDialog("myModal");><i class="fa fa-trash-o"></i> Cancel</button><label id="error" style="margin-left: 2%"></label></div></div>');
    $("#myModal").modal("show");
}

function onDeleteEmployee(emp_id) {
    var url = '../user/user_remove';
    $.post(url,{emp_id:emp_id},function (data) {

        var status = data.Status;
        var message = data.Message;
        if(status) {
            $("#myModal").modal("hide");
            getAllUsers();
        }
        else{
            $("#error").html(message);
            $("#error").css("color","red");
        }

    });
}
