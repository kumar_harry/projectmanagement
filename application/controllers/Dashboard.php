<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->model('ProjectModal');
        $this->load->library('validator');

    }
    public function index()
    {
        $session = $this->session->userdata('proj_is_logged');

        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/dashboard');
        }
        else{
            redirect('user/', 'refresh');

        }
    }
    public function getAllProjects(){
        $projectData = $this->ProjectModal->getProjects();
        if($projectData){
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Data Found Successfully';
            $response['AllProject'] = $projectData;
        }
        else{
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Error in data found';
        }
        $this->validator->apiResponse($response);
    }
    public function createProject(){
        $requiredfields = array('projectName','projectType','projectManager','projectLeader','projectTeam','startDate',
            'deadlineDate','closingDate','currentDate');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjName($this->input->post('projectName'));
        $this->ProjectModal->setProjType($this->input->post('projectType'));
        $this->ProjectModal->setProjManager($this->input->post('projectManager'));
        $this->ProjectModal->setProjTeamLeader($this->input->post('projectLeader'));
        $this->ProjectModal->setProjDeveloper($this->input->post('projectTeam'));
        $this->ProjectModal->setProjStartDate($this->input->post('startDate'));
        $this->ProjectModal->setProjDeadLine($this->input->post('deadlineDate'));
        $this->ProjectModal->setProjClosingDate($this->input->post('closingDate'));
        $this->ProjectModal->setCurrentDate($this->input->post('currentDate'));
        $isCreate = $this->ProjectModal->createProject();
        $status = $isCreate['Status'];
        if(!$status) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = $isCreate['Message'];
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = $isCreate['Message'];
        $response['proj_id'] = $isCreate['proj_id'];
        $this->validator->apiResponse($response);
        return true;
    }
    public function updateProject(){
        $requiredfields = array('projectName','projectType','projectManager','projectLeader','projectTeam','startDate',
            'deadlineDate','closingDate','proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjName($this->input->post('projectName'));
        $this->ProjectModal->setProjType($this->input->post('projectType'));
        $this->ProjectModal->setProjManager($this->input->post('projectManager'));
        $this->ProjectModal->setProjTeamLeader($this->input->post('projectLeader'));
        $this->ProjectModal->setProjDeveloper($this->input->post('projectTeam'));
        $this->ProjectModal->setProjStartDate($this->input->post('startDate'));
        $this->ProjectModal->setProjDeadLine($this->input->post('deadlineDate'));
        $this->ProjectModal->setProjClosingDate($this->input->post('closingDate'));
        $this->ProjectModal->setProjId($this->input->post('proj_id'));
        $isUpdate = $this->ProjectModal->updateProject();
        if(!$isUpdate) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = "error in update project";
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = "project updated successfully";
        $this->validator->apiResponse($response);
        return true;
    }
    public function deleteProject(){
        $requiredfields = array('proj_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();
        $status = $param_response[$this->config->item('status')];
        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }
        $this->ProjectModal->setProjId($this->input->post('proj_id'));
        $isDelete = $this->ProjectModal->deleteProject();
        if(!$isDelete) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = "error in delete project";
            $this->validator->apiResponse($response);
            return false;
        }
        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = "Project delete successfully.";
        $this->validator->apiResponse($response);
        return true;
    }
}
