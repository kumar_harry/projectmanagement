<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see https://codeigniter.com/user_guide/general/urls.html
     */


    public function __construct()
    {
        parent::__construct();
        $this->load->model('users_model');
        $this->load->library('validator');

    }
    public function index()
    {
        $this->load->view('backend/login');
    }
    public function users() {
        $session = $this->session->userdata('proj_is_logged');

        if($session) {
            $this->load->view('backend/header',array('session'=>$this->session->userdata()));
            $this->load->view('backend/employees');
        }
        else{
            redirect('user/', 'refresh');

        }
    }

    /*
     * this will login user
     *
     * */

    public function login() {
        $requiredfields = array('email_id','password');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();

        $status = $param_response[$this->config->item('status')];

        if (!$status ) {
            $this->validator->apiResponse($param_response);
            return false;
        }

        if(!$this->validator->isValidEmail($this->input->post('email_id'))) {
            $response[$this->config->item('status')] = $this->config->item('success');
            $response[$this->config->item('message')] = 'Please enter valid email-id';
            $this->validator->apiResponse($response);
            return false;
        }
        $this->users_model->setEmpEmail($this->input->post('email_id'));
        $this->users_model->setEmpPassword($this->input->post('password'));
        $isLogin = $this->users_model->login();

        if(!$isLogin) {
            $response[$this->config->item('status')] = $this->config->item('error');
            $response[$this->config->item('message')] = 'Please enter valid email-id and password';
            $this->validator->apiResponse($response);
            return false;
        }
        $this->session->set_userdata(array(
            'proj_emp_id'  => $isLogin[0]->emp_id,
            'proj_emp_name' => $isLogin[0]->emp_name,
            'proj_emp_email'  => $isLogin[0]->emp_email,
            'proj_emp_role'  => $isLogin[0]->emp_role,
            'proj_is_logged'     => true,
        ));

//        print_r($this->session->userdata('proj_emp_id'));

        $response[$this->config->item('status')] = $this->config->item('success');
        $response[$this->config->item('message')] = 'User logged in successfully';
        $this->validator->apiResponse($response);
        return false;

    }

    /*
     * this will gives users_data..
     *
     * */

    public function users_data() {

      $usersData = $this->users_model->getAllUsers();

      if($usersData) {
          $response[$this->config->item('status')] = true;
          $response[$this->config->item('message')] = 'User data found';
          $response["data"] = $usersData;
          return $this->validator->apiResponse($response);
      }
        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'User data not found';

        return $this->validator->apiResponse($response);

    }

    /*
     * this will create user data ...
     *
     * */

    public function user_create() {
        $requiredfields = array('emp_name','emp_email','emp_password','emp_desg','emp_role','emp_created_at','emp_status','emp_type');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();

        $status = $param_response[$this->config->item('status')];

        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }

        if(!$this->validator->isValidEmail($this->input->post('emp_email'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Please enter valid email-id';
            return $this->validator->apiResponse($response);
        }

        if($this->users_model->isUserExists()) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Email id already exists';
            return $this->validator->apiResponse($response);
        }

        $this->users_model->setEmpName($this->input->post('emp_name'));
        $this->users_model->setEmpEmail($this->input->post('emp_email'));
        $this->users_model->setEmpPassword($this->input->post('emp_password'));
        $this->users_model->setEmpCreatedAt($this->input->post('emp_created_at'));
        $this->users_model->setEmpDesignation($this->input->post('emp_desg'));
        $this->users_model->setEmpRole($this->input->post('emp_role'));
        $this->users_model->setEmpStatus($this->input->post('emp_status'));
        $this->users_model->setEmpType($this->input->post('emp_type'));

        if($this->users_model->save()) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee created successfully';
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Unable to create employee';

        return $this->validator->apiResponse($response);

    }

    /*
     * this will update user data ...
     *
     * */

    public function user_update() {

        $requiredfields = array('emp_name','emp_desg','emp_role','emp_status');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();

        $status = $param_response[$this->config->item('status')];

        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }

        if(!$this->validator->isValidEmail($this->input->post('emp_email'))) {
            $response[$this->config->item('status')] = false;
            $response[$this->config->item('message')] = 'Please enter valid email-id';
            return $this->validator->apiResponse($response);
        }

        $this->users_model->setEmpName($this->input->post('emp_name'));
        $this->users_model->setEmpDesignation($this->input->post('emp_desg'));
        $this->users_model->setEmpRole($this->input->post('emp_role'));
        $this->users_model->setEmpStatus($this->input->post('emp_status'));
        $this->users_model->setEmpId($this->input->post('emp_id'));

        if($this->users_model->update()) {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee updated successfully';
            return $this->validator->apiResponse($response);
        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Unable to update employee';

        return $this->validator->apiResponse($response);
    }

    /*
     * this will remove user data ...
     *
     * */

    public function user_remove() {
        $requiredfields = array('emp_id');
        $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
        $response = array();

        $status = $param_response[$this->config->item('status')];

        if (!$status ) {
            return $this->validator->apiResponse($param_response);
        }
        $this->users_model->setEmpId($this->input->post('emp_id'));
        if($this->users_model->remove())  {
            $response[$this->config->item('status')] = true;
            $response[$this->config->item('message')] = 'Employee removed successfully';
            return $this->validator->apiResponse($response);

        }

        $response[$this->config->item('status')] = false;
        $response[$this->config->item('message')] = 'Unable to remove employee';
        return $this->validator->apiResponse($response);

    }
}
