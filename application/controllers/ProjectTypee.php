<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 28-12-2017
 * Time: 12:34
 */
class ProjectTypee extends CI_Controller
{
 public function __construct()
 {
     parent::__construct();
 }
 public function index() {
     $this->load->view('backend/header');
     $this->load->view('backend/type');
 }

 /*
  * this will get all types data
  *
  * */

 public function types() {
   $response = array();
   $type_data = $this->db_results->get_data('tbl_project_type','*',array());
   if($type_data) {
       $response[$this->config->item('status')] = true;
       $response[$this->config->item('message')] = 'Type data found';
       $response["data"] = $type_data;
       return $this->validator->apiResponse($response);
   }

     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'Type data not found';
     return $this->validator->apiResponse($response);
 }

    /*
     * this will get create new type data
     *
     * */

    public function type_create() {
     $requiredfields = array('type_name','type_status','type_created_at');
     $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
     $response = array();

     $status = $param_response[$this->config->item('status')];

     if (!$status ) {
         return $this->validator->apiResponse($param_response);
     }
     $data = array('type_name'=>$this->input->post('type_name'),
                'type_status'=>$this->input->post('type_status'),
                'type_created_at'=>$this->input->post('type_created_at'));

     $ins_ = $this->db_results->post_data('tbl_project_type',$data);
     if($ins_) {
         $response[$this->config->item('status')] = true;
         $response[$this->config->item('message')] = 'Type created successfully';
         return $this->validator->apiResponse($response);
     }
     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'unable to create project type';
     return $this->validator->apiResponse($response);
 }
 /*
 * this will update type data
 *
 *
 * */

    public function type_update() {
     $requiredfields = array('type_id','type_name','type_status');
     $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
     $response = array();

     $status = $param_response[$this->config->item('status')];

     if (!$status ) {
         return $this->validator->apiResponse($param_response);
     }

     $data = array('type_name'=>$this->input->post('type_name'),
               'type_status'=>$this->input->post('type_status'));
     $where = array('type_id'=>$this->input->post('type_id'));

     $update_ = $this->db_results->update_data('tbl_project_type',$data,$where);

     if($update_) {
         $response[$this->config->item('status')] = true;
         $response[$this->config->item('message')] = 'project type updated successfully';
         return $this->validator->apiResponse($response);

     }
     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'unable to update project type';
     return $this->validator->apiResponse($response);
 }

 /*
 * this will remove type data
 *
 *
 * */
 public function type_remove() {
     $requiredfields = array('type_id');
     $param_response = $this->validator->valid_params( $this->input->post() , $requiredfields);
     $response = array();

     $status = $param_response[$this->config->item('status')];

     if (!$status ) {
         return $this->validator->apiResponse($param_response);
     }
     $where = array('type_id'=>$this->input->post('type_id'));

     $remove_ = $this->db_results->delete_data('tbl_project_type',$where);
     if($remove_) {
         $response[$this->config->item('status')] = true;
         $response[$this->config->item('message')] = 'project type removed successfully';
         return $this->validator->apiResponse($response);

     }
     $response[$this->config->item('status')] = false;
     $response[$this->config->item('message')] = 'unable to remove project type';

     return $this->validator->apiResponse($response);
 }

}