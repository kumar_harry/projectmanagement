<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 26-12-2017
 * Time: 18:01
 */
class Users_model extends CI_Model
{
    private $emp_id = null;
    private $emp_name = null;
    private $emp_designation = null;
    private $emp_role = null;
    private $emp_created_at = null;
    private $emp_updated_at = null;
    private $emp_status = null;
    private $emp_email = null;
    private $emp_password = null;
    private $emp_type = null;

    public function __construct()
    {
        parent::__construct();
    }

    function login()
    {
        $this -> db -> select('emp_id,emp_name, emp_email,emp_role');
        $this -> db -> from('tbl_employees');
        $this -> db -> where('emp_email = ' . "'" . $this->emp_email . "'");
        $this -> db -> where('emp_password = ' . "'" . MD5($this->emp_password) . "'");
        $this -> db -> limit(1);
        $query = $this -> db -> get();
        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        else
        {
            return false;
        }

    }

    function getAllUsers() {
        $this -> db -> select('*');
        $this -> db -> from('tbl_employees');
        $query = $this -> db -> get();

        if($query -> num_rows() >0)
        {
            return $query->result();
        }
        return false;
    }

    function isUserExists() {
        $this -> db -> select('emp_id');
        $this -> db -> from('tbl_employees');
        $this -> db -> where('emp_email = ' . "'" . $this->emp_email . "'");
        $this -> db -> where('emp_status = ' . "'A'");

        $query = $this -> db -> get();
        if($query -> num_rows() == 1)
        {
            return $query->result();
        }
        return false;
    }

    function save() {
        $data = array(
            'emp_name' => $this->emp_name,
            'emp_email' => $this->emp_email,
            'emp_password' => md5($this->emp_password),
            'emp_designation' => $this->emp_designation,
            'emp_role' => $this->emp_role,
            'emp_created_at' => $this->emp_created_at,
            'emp_status' => $this->emp_status,
            'emp_type' => $this->emp_type
        );
        $this->db->insert('tbl_employees', $data);
        $proj_id = $this->db->insert_id();
        if($proj_id>0) {
            return true;
        }
        return false;
    }

    function update() {
        $data = array(
            'emp_name' => $this->emp_name,
            'emp_designation' => $this->emp_designation,
            'emp_role' => $this->emp_role,
            'emp_status' => $this->emp_status
        );
        $this->db->where('emp_id',$this->emp_id);
        $this->db->update('tbl_employees', $data);

        $afftectedRows = $this->db->affected_rows();
        if($afftectedRows>0) {
            return true;
        }
        return false;
    }

    function remove() {
        $this->db->where('emp_id', $this->emp_id);
        $this->db->delete('tbl_employees');
        $afftectedRows = $this->db->affected_rows();

        if($afftectedRows>0) {
            return true;
        }
        return false;
    }

    /**
     * @param null $emp_created_at
     */
    public function setEmpCreatedAt($emp_created_at)
    {
        $this->emp_created_at = $emp_created_at;
    }

    /**
     * @param null $emp_designation
     */
    public function setEmpDesignation($emp_designation)
    {
        $this->emp_designation = $emp_designation;
    }

    /**
     * @param null $emp_email
     */
    public function setEmpEmail($emp_email)
    {
        $this->emp_email = $emp_email;
    }

    /**
     * @param null $emp_id
     */
    public function setEmpId($emp_id)
    {
        $this->emp_id = $emp_id;
    }

    /**
     * @param null $emp_name
     */
    public function setEmpName($emp_name)
    {
        $this->emp_name = $emp_name;
    }

    /**
     * @param null $emp_password
     */
    public function setEmpPassword($emp_password)
    {
        $this->emp_password = $emp_password;
    }

    /**
     * @param null $emp_role
     */
    public function setEmpRole($emp_role)
    {
        $this->emp_role = $emp_role;
    }

    /**
     * @param null $emp_status
     */
    public function setEmpStatus($emp_status)
    {
        $this->emp_status = $emp_status;
    }

    /**
     * @param null $emp_type
     */
    public function setEmpType($emp_type)
    {
        $this->emp_type = $emp_type;
    }

    /**
     * @param null $emp_updated_at
     */
    public function setEmpUpdatedAt($emp_updated_at)
    {
        $this->emp_updated_at = $emp_updated_at;
    }

}