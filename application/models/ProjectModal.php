<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 12/26/2017
 * Time: 7:03 PM
 */
class ProjectModal extends CI_Model
{
    private $result_array = array();
    private $proj_id = null;
    private $proj_name = null;
    private $proj_type = null;
    private $proj_manager = null;
    private $proj_team_leader = null;
    private $proj_developer = null;
    private $proj_start_date = null;
    private $proj_dead_line = null;
    private $proj_closing_date = null;
    private $currentDate = null;
    public function __construct()
    {
        parent::__construct();
    }
    function getProjects(){
        $this->db->select("proj_id,proj_name,proj_type,proj_manager,proj_team_leader,proj_developer,proj_start_date,
                           proj_dead_line,proj_closing_date,proj_created_at,proj_updated_at");
        $this->db->from("tbl_projects");
        $query = $this->db->get();
        if($query){
            return  $query->result();
        }
        else{
            return  false;
        }
    }
    function createProject(){
        if($this->projectExist()){
            $data = array(
                'proj_name' => $this->proj_name,
                'proj_type' => $this->proj_type,
                'proj_manager'=> $this->proj_manager,
                'proj_team_leader'   => $this->proj_team_leader,
                'proj_developer'  => $this->proj_developer,
                'proj_start_date'  => $this->proj_start_date,
                'proj_dead_line'  => $this->proj_dead_line,
                'proj_closing_date'  => $this->proj_closing_date,
                'proj_created_at'  => $this->currentDate,
            );
            $this->db->insert('tbl_projects', $data);
            $proj_id = $this->db->insert_id();
            $result_array[$this->config->item('status')] = true;
            $result_array[$this->config->item('message')] = "Project successfully created";
            $result_array['proj_id'] = $proj_id;
            return $result_array;
        }
        else{
            $result_array[$this->config->item('status')] = false;
            $result_array[$this->config->item('message')] = "Project name allready exist";
            return $result_array;
        }
    }
    function updateProject(){
            $data = array(
                'proj_name' => $this->proj_name,
                'proj_type' => $this->proj_type,
                'proj_manager'=> $this->proj_manager,
                'proj_team_leader'   => $this->proj_team_leader,
                'proj_developer'  => $this->proj_developer,
                'proj_start_date'  => $this->proj_start_date,
                'proj_dead_line'  => $this->proj_dead_line,
                'proj_closing_date'  => $this->proj_closing_date,
                'proj_id'  => $this->proj_id,
            );
            $this->db->where('proj_id',$this->proj_id);
            $this->db->update('tbl_projects', $data);
            $afftectedRows = $this->db->affected_rows();
            if($afftectedRows>0) {
                return true;
            }
            return false;

    }
    function projectExist(){
        $this->db->select("proj_name,proj_type");
        $this->db->from("tbl_projects");
        $this->db->where('proj_name', $this->proj_name);
        $this->db->where('proj_type',$this->proj_type );
        $query = $this->db->get();
        if ( $query->num_rows() > 0 )
        {
            $row = $query->row_array();
            return false;
        }
        return true;

    }
    function deleteProject(){
        $this->db->where('proj_id', $this->proj_id);
        $this->db->delete('tbl_projects');
        $afftectedRows = $this->db->affected_rows();
        if($afftectedRows>0) {
            return true;
        }
        return false;
    }

    /**
     * @param null $currentDate
     */
    public function setCurrentDate($currentDate)
    {
        $this->currentDate = $currentDate;
    }
    /**
     * @param null $proj_id
     */
    public function setProjId($proj_id)
    {
        $this->proj_id = $proj_id;
    }

    /**
     * @param null $proj_name
     */
    public function setProjName($proj_name)
    {
        $this->proj_name = $proj_name;
    }

    /**
     * @param null $proj_closing_date
     */
    public function setProjClosingDate($proj_closing_date)
    {
        $this->proj_closing_date = $proj_closing_date;
    }

    /**
     * @param null $proj_dead_line
     */
    public function setProjDeadLine($proj_dead_line)
    {
        $this->proj_dead_line = $proj_dead_line;
    }

    /**
     * @param null $proj_developer
     */
    public function setProjDeveloper($proj_developer)
    {
        $this->proj_developer = $proj_developer;
    }

    /**
     * @param null $proj_manager
     */
    public function setProjManager($proj_manager)
    {
        $this->proj_manager = $proj_manager;
    }

    /**
     * @param null $proj_start_date
     */
    public function setProjStartDate($proj_start_date)
    {
        $this->proj_start_date = $proj_start_date;
    }

    /**
     * @param null $proj_team_leader
     */
    public function setProjTeamLeader($proj_team_leader)
    {
        $this->proj_team_leader = $proj_team_leader;
    }

    /**
     * @param null $proj_type
     */
    public function setProjType($proj_type)
    {
        $this->proj_type = $proj_type;
    }





}