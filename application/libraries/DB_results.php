<?php

/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 28-12-2017
 * Time: 12:20
 */
class DB_results
{
  function __construct()
  {
      $this->CI =& get_instance();
      $this->CI->load->database();
  }

    public function get_data($table_name,$params,$where) {
        $this->CI -> db -> select($params);
        $this->CI -> db -> from($table_name);
        $this->CI ->db-> where($where);
        $query = $this->CI -> db -> get();

      if($query -> num_rows() >0)
      {
          return $query->result();
      }
      return false;
  }

  public function post_data($table_name,$data) {
      $this->CI -> db->insert($table_name, $data);
      $proj_id = $this->CI -> db->insert_id();
      if($proj_id>0) {
          return true;
      }
      return false;
  }

  public function update_data($table_name,$data,$where) {
      $this->CI -> db->where($where);
      $this->CI -> db->update($table_name, $data);
        $afftectedRows = $this->CI -> db->affected_rows();
        if($afftectedRows>0) {
            return true;
        }
     return false;
  }

  public function delete_data($table_name,$where) {

      $this->CI -> db->where($where);
      $this->CI -> db->delete($table_name);
      $afftectedRows = $this->db->affected_rows();

      if($afftectedRows>0) {
          return true;
      }
      return false;
  }

}